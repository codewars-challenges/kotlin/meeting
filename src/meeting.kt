import java.util.stream.Collectors.joining

fun meeting(s: String): String =
    s.toUpperCase()
        .split(";")
        .stream()
        .map { s -> s.split(":") }
        .sorted { o1, o2 -> if (o1[1].compareTo(o2[1]) != 0) o1[1].compareTo(o2[1]) else o1[0].compareTo(o2[0]) }
        .map { s -> "(${s.reversed().joinToString(", ")})" }
        .collect(joining())

fun main() {
    println(meeting("Fred:Corwill;Wilfred:Corwill;Barney:Tornbull;Betty:Tornbull;Bjon:Tornbull;Raphael:Corwill;Alfred:Corwill"))
}
